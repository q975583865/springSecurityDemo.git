package cn.test.mapper;

import cn.test.bean.Auth;
import cn.test.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserMapper extends tk.mybatis.mapper.common.Mapper<User> {
    User selectByFF(String id);

    List<Auth> getUserAuth(String l);
}