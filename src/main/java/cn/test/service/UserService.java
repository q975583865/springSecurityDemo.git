package cn.test.service;

import cn.test.bean.Auth;
import cn.test.bean.User;
import cn.test.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public List<Auth> getUserAuth(String l) {
        List<Auth> userAuth = this.userMapper.getUserAuth(l);
        return userAuth;
    }

    public User getUserByName(String username) {
        return this.userMapper.selectByFF(username);
    }
}