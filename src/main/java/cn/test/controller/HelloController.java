package cn.test.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class HelloController {

    //获取当前登录用户的权限集
    @GetMapping("/getCurrentUserAuth")
    public void getCurrentUserAuth() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();

        System.out.println("当前账号用户名："+userDetails.getUsername());
        for (GrantedAuthority grantedAuthority : authorities) {
            System.out.println("当前账号权限集：" + grantedAuthority.getAuthority());
        }
    }

    @PreAuthorize("hasAuthority('UserIndex')")
    @GetMapping("/one")
    public String one() {
        return "OK";
    }

    //常用 @PreAuthorize("hasAuthority('')")
    @PreAuthorize("hasAuthority('caidan')")
    @GetMapping("/two")
    public String two() {
        return "OK";
    }

    //满足一个权限就可以了，不常用
    @PreAuthorize("hasAnyAuthority('caidan','UserIndex')")
    @GetMapping("/three")
    public String three() {
        return "OK";
    }
}
